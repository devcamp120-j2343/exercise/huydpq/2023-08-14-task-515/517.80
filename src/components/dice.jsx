import { Component } from "react";
import Dice1 from "../assets/images/1.png"
import Dice2 from "../assets/images/2.png"
import Dice3 from "../assets/images/3.png"
import Dice4 from "../assets/images/4.png"
import Dice5 from "../assets/images/5.png"
import Dice6 from "../assets/images/6.png"
import Dice0 from "../assets/images/dice.png"

const diceImages = [
    Dice0,Dice1, Dice2, Dice3, Dice4, Dice5, Dice6
]
class Dice extends Component {
    constructor(props){
        super(props)
        this.state = {
            dice: Dice0
        }
    }

    rollDice = () => {
        let random = Math.floor(Math.random() * 6) + 1
        this.setState ({
            dice: diceImages[random]
        })
    }

    render() {
        
        return (
            <>
            <div>
            <img src={this.state.dice} alt="dice" width={100} height={100}/>
            </div>
            <button onClick={this.rollDice}>Ném</button>
            </>
        )
    }
}

export default Dice